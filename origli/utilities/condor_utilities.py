#!/usr/bin/env python

"""

 Script name: condor_utilities.py

 Description:
    File containing utilities




"""
try:
    import cv2
except:
    pass
import json
import numpy as np
import os



__author__ = "Kentaro Mogushi"
__copyright__ = "Copyright 2019-2025, Kentaro Mogushi"
__credits__ = ["Line for credits"]
__license__ = "GPL"
__version__ = "0.1.0"
__maintainer__ = "K. Mogushi"
__email__ = "kmhhz@umsystem.edu"
__status__ = "Developing"



class CondorUtils():

    def __init__(self):
        pass


    def create_condor_submission_file(self, work_dir, abs_path_executable, abs_path_config, obs_run='o3'):

        sub_file = 'trigger_generation.sub'
        sub_path = os.path.join(work_dir, sub_file)
        with open(sub_path, 'w') as f:
            f.write('universe = vanilla\n')
            f.write('getenv = True\n')
            f.write('executable = {}\n'.format(abs_path_executable))
            f.write('log = {}/logs/$(job_id).log\n'.format(work_dir))
            f.write('error = {}/logs/$(job_id).err\n'.format(work_dir))
            f.write('output = {}/logs/$(job_id).out\n'.format(work_dir))
            f.write('arguments = -c {} --condor --job-id $(job_id)\n'.format(abs_path_config))
            f.write('notification = error\n')
            f.write('accounting_group = ligo.dev.{}.detchar.explore.test\n'.format(obs_run))
            f.write('request_memory = 5 GB\n')
            f.write('queue 1')
        return sub_path


    def create_condor_dag_file(self, path_sub, work_dir, num_glitches_to_be_analyzed):

        dag_file = 'trigger_generation.dag'
        dag_path = os.path.join(work_dir, dag_file)

        with open(dag_path, 'w') as f:
            for job_id in range(1, num_glitches_to_be_analyzed+1)[:]:
                dag_line = 'JOB {JobID} {PathSub} \nVARS {JobID} job_id="{JobID:06d}"\n'.format(
                    JobID=job_id,
                    PathSub=path_sub)
                f.write(dag_line)
                f.write("\n")

        return dag_path



