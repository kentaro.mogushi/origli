"""

 Script name: const.py

 Description:
    File containing the list of glitch names
    This is supposed to be used by import_data_hdf5.py under bin dir

"""


__author__ = "Kentaro Mogushi"
__maintainer__ = "K. Mogushi"
__email__ = "mogushi328@gmail.com"



#####################################################
#  The list of glitch names sorted by GravitySpy    #
#####################################################

# the list of glitches for (L1/H1/V1)

# for L1
L1Glitches = ['Extremely_Loud',
              'Low_Frequency_Burst',
              'Scattered_Light',
              'Whistle',
              'Violin_Mode',
              'Scratchy',
              'Power_Line',
              'Helix',
              'Repeating_Blips',
              'Blip',
              'Koi_Fish',
              'Low_Frequency_Lines',
              'Light_Modulation',
              '1400Ripples',
              'None_of_the_Above',
              'Wandering_Line',
              'Chirp',
              'No_Glitch',
              'Air_Compressor',
              'Tomte',
              '1080Lines',
              'Paired_Doves',
              'Loud',
              'Noise',
              'Repeating',
              'SineGaussian',
              'Needle',
              'Scattering',
              'Line',
              'Magnetometer',
              'unknown',
              'arbitrary',
              'Fast_Scattering']

# for H1
H1Glitches = ['Extremely_Loud',
              'Low_Frequency_Burst',
              'Scattered_Light',
              'Whistle',
              'Violin_Mode',
              'Scratchy',
              'Power_Line',
              'Helix',
              'Repeating_Blips',
              'Blip',
              'Koi_Fish',
              'Low_Frequency_Lines',
              'Light_Modulation',
              '1400Ripples',
              'None_of_the_Above',
              'Wandering_Line',
              'Chirp',
              'No_Glitch',
              'Air_Compressor',
              'Tomte',
              '1080Lines',
              'Paired_Doves',
              'Loud',
              'Noise',
              'Repeating',
              'Scattered',
              'HighF',
              'unknown',
              'arbitrary',
              'Fast_Scattering']

# for V1
V1Glitches = ['Extremely_Loud',
              'Repeating_Blips',
              'Scratchy',
              'Wandering_Line',
              'Blip',
              'Koi_Fish',
              'Low_Frequency_Burst',
              'Light_Modulation',
              'Scattered_Light',
              'Violin_Mode',
              'None_of_the_Above',
              '1080Lines',
              'Tomte',
              'No_Glitch',
              'Low_Frequency_Lines',
              'Whistle',
              'Power_Line',
              'Helix',
              '1400Ripples',
              'unknown',
              'arbitrary',
              'Fast_Scattering']


# 50 safe channels tha are used to create a small time series dataset 
re_sfchs_sub = ['OMC-ASC_POS_Y_OUT_DQ', 'SUS-ITMX_L2_WIT_Y_DQ',
                 'ASC-SRC1_P_OUT_DQ', 'SUS-SR2_M3_NOISEMON_UR_OUT_DQ',
                'PEM-CS_SEIS_LVEA_VERTEX_QUAD_SUM_DQ',
                'HPI-ETMX_BLND_L4C_VP_IN1_DQ', 'SUS-ETMX_M0_DAMP_V_IN1_DQ',
                'SUS-PRM_M3_NOISEMON_LR_OUT_DQ', 'PEM-EY_VAULT_SEIS_STS2_Y_DQ',
                'SUS-MC3_M3_NOISEMON_LR_OUT_DQ', 'SUS-ETMX_M0_DAMP_T_IN1_DQ',
                'SUS-ITMY_M0_DAMP_V_IN1_DQ', 'PEM-CS_ACC_LVEAFLOOR_XCRYO_Z_DQ',
                'ISI-ETMY_ST1_BLND_RY_T240_CUR_IN1_DQ', 'SUS-ITMY_L1_WIT_Y_DQ',
                'PEM-CS_MAG_LVEA_OUTPUTOPTICS_Y_DQ',
                'SUS-MC2_M3_NOISEMON_UR_OUT_DQ', 'SUS-SRM_M1_DAMP_Y_IN1_DQ',
                'LSC-MCL_IN1_DQ', 'HPI-HAM3_BLND_L4C_RX_IN1_DQ',
                'SUS-SRM_M3_WIT_P_DQ', 'ASC-SRC2_Y_OUT_DQ',
                'SUS-IM2_M1_DAMP_Y_IN1_DQ', 'SUS-PR3_M1_DAMP_T_IN1_DQ',
                'SUS-MC3_M3_NOISEMON_UL_OUT_DQ', 'SUS-SRM_M3_NOISEMON_UL_OUT_DQ',
                'ISI-ITMX_ST2_BLND_RX_GS13_CUR_IN1_DQ',
                'PEM-EY_VAULT_MAG_LEMI_Y_DQ', 'HPI-HAM1_BLND_L4C_Z_IN1_DQ',
                'SUS-MC2_M2_NOISEMON_LL_OUT_DQ', 'SUS-OMC_M1_NOISEMON_RT_OUT_DQ',
                'SUS-ITMX_M0_DAMP_Y_IN1_DQ', 'SUS-BS_M1_NOISEMON_RT_OUT_DQ',
                'SUS-PR3_M3_OPLEV_YAW_OUT_DQ', 'CAL-PCALX_TX_PD_OUT_DQ',
                'ASC-REFL_A_RF9_Q_YAW_OUT_DQ', 'HPI-ETMX_BLND_L4C_Y_IN1_DQ',
                'IMC-DOF_4_Y_IN1_DQ', 'ASC-Y_TR_A_NSUM_OUT_DQ',
                'SUS-PRM_M1_DAMP_Y_IN1_DQ', 'SUS-SR2_M3_WIT_P_DQ',
                'IMC-WFS_B_Q_PIT_OUT_DQ', 'HPI-BS_BLND_L4C_Z_IN1_DQ',
                'ISI-BS_ST1_BLND_Z_T240_CUR_IN1_DQ',
                'SUS-MC2_M3_NOISEMON_LL_OUT_DQ', 'ISI-HAM4_BLND_GS13RX_IN1_DQ',
                'SUS-PRM_M2_NOISEMON_UR_OUT_DQ', 'PEM-CS_ACC_HAM5_SRM_X_DQ',
                'SUS-PR2_M1_DAMP_P_IN1_DQ', 'SUS-ETMY_M0_DAMP_V_IN1_DQ']


# frequency bands
freq_bands = [[1, 50],
              [1, 128],
              [128, 256],
              [256, 512],
              [512, 1024],
              [1024, 2048],
              [2048, 4096],
              [4096, 8192],
              ['None', 'None']]

# freq_bands = [[1, 128],
#               [128, 256],
#               [256, 512],
#               [512, 1024],
#               [1024, 2048],
#               [2048, 4096],
#               [4096, 8192],
#               ['None', 'None']]


